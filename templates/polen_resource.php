<?php
get_header();
global $yoqex_polen;
global $post;
?>

<article id="<?php echo $post->post_type . "-" .  $post->ID; ?>" class="<?php echo $post->post_type; ?> type-<?php echo $post->post_type; ?> status-<?php echo $post->post_status; ?>  hentry " itemscope="" itemtype="http://schema.org/Article" itemprop="mainEntity">
    <?php
    if (isset($yoqex_polen)) {
        echo $yoqex_polen->cp_Polen_Resource->get_display_grid([
            "p" => $post->ID
        ]);
    }
    ?>
</article>

<?php
get_footer();
?>