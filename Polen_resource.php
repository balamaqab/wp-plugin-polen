<?php
if (!class_exists('Yoqex_Polen_Resource')) {
    class Yoqex_Polen_Resource
    {
        public $domain;
        public $qty_resources = 8;
        public $custom_post_type = "polen_resource";
        public $prefix = "yq_polen_";
        function __construct($args = array())
        {

            if (isset($args["domain"])) {
                $domain = $args["domain"];
            }

            add_action('admin_enqueue_scripts',         array($this, 'admin_styles'));
            add_action('admin_head',                    array($this, 'remove_media_buttons'));
            add_action('admin_init',                    array($this, 'remove_meta_boxes'));
            add_action('add_meta_boxes',                array($this, 'add_metabox_links'));
            add_action('save_post',                     array($this, 'save_links'));

            add_filter("fb_og_metabox_exclude_types",   array($this, 'remove_fb_og_metabox'));
            add_filter('single_template', array($this, 'get_single_page_layout'));

            $this->create();
        }

        function create()
        {
            $labels = array(
                'name'               => __('Recursos', $this->domain),
                'singular_name'      => __('Recurso', $this->domain),
                'add_new'            => __('Agregar', $this->domain),
                'add_new_item'       => __('Agregar recurso', $this->domain),
                'edit_item'          => __('Editar recurso', $this->domain),
                'new_item'           => __('Nuevo recurso', $this->domain),
                'view_item'          => __('Ver recurso', $this->domain),
                'search_items'       => __('Buscar recurso', $this->domain),
                'not_found'          => __('Recurso no encontrado', $this->domain),
                'not_found_in_trash' => __('Recurso no encontrado en la papelera ', $this->domain),
            );
            $args   = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_in_rest'       => false, // Adds gutenberg support.
                'show_ui'            => true,
                'query_var'          => true,
                'rewrite'            => array(
                    'slug'       => _x('resource', 'slug', $this->domain),
                    'with_front' => false,
                ),
                'has_archive'        => false,
                'capability_type'    => 'page',
                'hierarchical'       => true,
                'menu_position'      => 20,
                'menu_icon'          => 'dashicons-format-gallery',
                'supports'           => array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions'),
            );
            register_post_type($this->custom_post_type, $args);
        }

        public function get($custom_query = array())
        {
            $default_query = array(
                'post_type'         => $this->custom_post_type,
                'post_status'       => 'publish',
                'numberposts'       => -1,
                'order'             => 'DESC',
                'orderby'           => 'date',
            );
            $query = array_merge($default_query, $custom_query);
            $results = get_posts($query);

            $cfields = [$this->prefix . "label", $this->prefix . "link"];

            foreach ($results as $result) {
                $id = $result->ID;
                $result->image_large = get_the_post_thumbnail_url($id, 'large');
                $result->image_medium = get_the_post_thumbnail_url($id, 'medium');
                $result->image_small = get_the_post_thumbnail_url($id, 'thumbnail');
                foreach ($cfields as $cfield) {
                    $result->{$cfield} =  json_decode(get_post_meta($id,  $cfield, true));
                }
            }

            return $results;
        }

        public function get_single_page_layout($single)
        {
            global   $post;
            if ($this->custom_post_type === $post->post_type) {
                return plugin_dir_path(__FILE__) . '/templates/polen_resource.php';
            }
            return $single;
        }

        public function get_display_grid($custom_query = array(), $attrs = array(), $content = null)
        {

            $data = [
                "items" => (array) $this->get($custom_query),
                "css_prefix" => $this->prefix,
                "attributes" =>  $attrs,
            ];

            $loader = new \Twig\Loader\FilesystemLoader(plugin_dir_path(__FILE__) . '/templates/');
            $twig = new \Twig\Environment($loader);
            $content = $twig->render('shortcode-grid.html', $data);

            return $content;
        }

        public function admin_styles()
        {
            wp_enqueue_style($this->prefix . 'admin_styles',  plugins_url('/styles/polen-dashboard.css', __FILE__));
        }

        public function remove_meta_boxes()
        {
            remove_meta_box('pageparentdiv', $this->custom_post_type, 'side');
            remove_meta_box('sharing_meta', $this->custom_post_type, 'side');
            remove_meta_box('slugdiv', $this->custom_post_type, 'side');
        }

        public function remove_fb_og_metabox($types)
        {
            $types[] = $this->custom_post_type;
            return $types;
        }

        public function remove_media_buttons()
        {
            global $current_screen;
            if ($this->custom_post_type == $current_screen->post_type) {
                remove_action('media_buttons', 'media_buttons');

                global $cryout_serious_slider;
                if (isset($cryout_serious_slider)) {
                    remove_action('media_buttons', array($cryout_serious_slider, 'media_slider_button'));
                }
            }
        }

        public function add_metabox_links()
        {
            add_meta_box($this->prefix . 'metabox_links', 'Enlaces', array($this, 'metabox_links'), $this->custom_post_type, 'normal', 'low');
        }

        function prepare_links_fields($labels = array(), $links = array())
        {
            $label_placeholder = [
                "Ver presentacion",
                "Ver video",
                "Ver galería de fotos",
                "Descargar trifoliar",
                "Descargar carta de solicitud"
            ];

            $rows = array();
            for ($f = 0; $f < $this->qty_resources; $f++) {
                $rows[] = [
                    "fields" => [
                        // Label
                        [
                            "id" => $this->prefix . "label_" . $f,
                            "name" => $this->prefix . "label[]",
                            "class" => $this->prefix . "field_label_" . $f . " " . $this->prefix . "field_1_25",
                            "label" => __("Etiqueta", $this->domain),
                            "label_class" =>  $this->prefix . "label_" . $f . "_lbl",
                            "input_class" => $this->prefix . "label_" . $f . "_txt",
                            "type" => "text",
                            "placeholder" => isset($label_placeholder[$f]) ? $label_placeholder[$f] : "",
                            "value" => isset($labels[$f]) ? $labels[$f] : "",
                        ],
                        // Links
                        [
                            "id" => $this->prefix . "link_" . $f,
                            "name" => $this->prefix . "link[]",
                            "class" => $this->prefix . "field_link_" . $f,
                            "label" => __("Enlace", $this->domain),
                            "label_class" => $this->prefix . "link_" . $f . "_lbl",
                            "input_class" => $this->prefix . "link_" . $f . "_txt",
                            "placeholder" => "https://",
                            "type" => "url",
                            "value" =>  isset($links[$f]) ? $links[$f] : "",
                        ],
                    ]
                ];
            }
            return $rows;
        }

        public function metabox_links($post)
        {
            $labels = json_decode(get_post_meta($post->ID, $this->prefix . 'label', true));
            $links = json_decode(get_post_meta($post->ID, $this->prefix . 'link', true));


            $data = [
                "css_prefix" =>  $this->prefix,
                "rows" => $this->prepare_links_fields($labels, $links)
            ];

            $twig = new \Twig\Environment(new \Twig\Loader\FilesystemLoader(plugin_dir_path(__FILE__) . '/templates/'));
            echo $twig->render('metabox-links.html', $data);
        }


        public function save_links($post_id)
        {
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

            if ($parent_id = wp_is_post_revision($post_id)) {
                $post_id = $parent_id;
            }
            $fields = [
                $this->prefix . 'label',
                $this->prefix . 'link',
            ];

            foreach ($fields as $field) {
                if (array_key_exists($field, $_POST)) {
                    $field_content = $_POST[$field];
                    if (is_array($field_content)) {
                        $field_content =  json_encode($field_content, JSON_UNESCAPED_UNICODE);
                    }
                    update_post_meta($post_id, $field, sanitize_text_field($field_content));
                }
            }
        }
    }
}
