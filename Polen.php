<?php

/**
 * Plugin Name: Polen
 * Plugin URI:  https://gitlab.com/yoqex/polen
 * Description: Manage a collection of resources, as documents and links, to build a mediakit page for your organization.
 * Version:     0.9.1
 * Author:      imendoza
 * Author URI:  https://ivanmendoza.net
 * Text Domain: yoqex_polen
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package     Polen
 * @author      imendoza
 * @copyright   2022 Yo'o Guatemala
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 *
 * Prefix:      Plugin Functions Prefix
 */

defined('ABSPATH') || die('No script kiddies please!');

require_once 'vendor/autoload.php';
include_once("Polen_resource.php");

if (!class_exists('Yoqex_Polen')) {
    class Yoqex_Polen
    {

        static $instance = false;
        public $domain = "yoqex_polen";
        public $version = "0.9.1";
        public $cp_Polen_Resource;

        function __construct()
        {
            add_action('init',                      array($this, 'custom_type_creation'));
            add_action('plugins_loaded',            array($this, 'textdomain'));
            add_shortcode('polen_grid',             array($this, 'shortcode_polengrid'));
            add_action('wp_enqueue_scripts',        array($this, 'enqueue_styles'),    10);
        }

        public function textdomain()
        {
            load_plugin_textdomain($this->domain, false, plugin_dir_path(__FILE__) . '/languages');
        }

        public function enqueue_styles()
        {
            wp_enqueue_style($this->domain, plugins_url('/styles/polen.css', __FILE__), array(), $this->version);
        }

        public function custom_type_creation()
        {
            $this->cp_Polen_Resource = new Yoqex_Polen_Resource(["domain" => $this->domain]);
        }


        public function shortcode_polengrid($attributes, $content = null)
        {
            $query = array();
            foreach ($attributes as $k => $attribute) {

                switch ($k) {
                    case "filter":
                        $query["s"] = $attributes["filter"];
                        break;
                    case "order":
                        $query["order"] = strtoupper($attributes["order"]);
                        break;
                    case "orderby":
                        $query["orderby"] = $attributes["orderby"];
                        break;
                }
            }

            return $this->cp_Polen_Resource->get_display_grid($query, $attributes, $content);
        }
    }

    $yoqex_polen = new Yoqex_Polen();
}
